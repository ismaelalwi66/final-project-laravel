@extends('layout.master')


@section('title')
  following
@endsection


@section('content')

<section class="content pt-4">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  @if($profile->image == '')
                  <img src="{{asset('/adminlte/dist/img/avatar3.png')}}" class="profile-user-img img-fluid img-circle" alt="User Image">
                  @else
                  <img class="profile-user-img img-fluid img-circle" src="{{asset('profiles/images/'.$profile->image)}}" alt="User profile picture">
                  @endif
                </div>

                <h3 class="profile-username text-center">{{ $profile->name }}</h3>

                <p class="text-muted text-center">{{ $profile->user->username }}</p>

                <ul class="list-group list-group-unbordered mb-3">
                  <li class="list-group-item">
                      <b>Followers</b> <a class="float-right btn btn-primary btn-sm" href="{{ route('followers.followers', $profile->user->username) }}"> {{count($follower)}} </a>
                    </li>
                    <li class="list-group-item">
                      <b>Following</b> <a class="float-right btn btn-primary btn-sm" href="{{ route('following.following', $profile->user->username) }}"> {{count($following)}} </a>
                    </li>
                </ul>
                <a href="{{ route('profile.index') }}" class="btn btn-primary btn-block">Lihat Profile</a>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->


            <!-- /.card -->
          </div>

          <div class="col-md-9">
            <div class="card card-primary card-outline">
              <!-- /.card-header -->
              <div class="card-body p-2">
                <div class="table-responsive mailbox-messages">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>#</th>
                      <th>Nama</th>
                      <th>Skill</th>
                      <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>

                       @if (count($following) == 0)
                        <tr>
                          <td align="center" colspan="4">No Following</td>
                        </tr>
                      @endif
                      
                        @foreach($data as $item)

                      @foreach ($following as $follow)

                          @if($item->id == $follow->following_user_id)

                          <tr>
                              <td class="align-middle">
                              <a href="{{ route('friend-details', $item->user->id) }}" class="user-block">
                                  @if ($item->image != "")
                                  <img src="{{asset('profiles/images/'.$item->image)}}" class="img-circle img-bordered-sm" alt="User Image">
                                  @else
                                  <img src="{{asset('profiles/images/user.png')}}" class="img-circle img-bordered-sm" alt="User Image">
                                  @endif
                              </a>
                              </td>
                              <td class="align-middle"><a href="{{ route('friend-details', $item->user->id) }}" class="">
                                  @if ($item->name != "")
                                  <b>{{ $item->name }}</b></a>
                                  @else
                                  <b>{{ $item->user->username }}</b></a>
                                  @endif
                              </td>
                              <td class="align-middle">{{ $item->skill }}</td>
                              <td class="align-middle">
                              @if(Auth::user()->id != $item->id)
                              <form action="{{ route('following.store', $item) }}" method="post">
                                  @csrf
                                  @if(Auth::user()->follows()->where('following_user_id', $item->id)->first())
                                  <button type="submit" class="btn btn-primary btn-block" value="Follow">Unfollow</button>
                                  @else
                                  <button type="submit" class="btn btn-primary btn-block" value="Follow">Follow</button>
                                  @endif
                              </form>
                              @endif
                              </td>
                              </tr>

                          @endif

                      @endforeach

                    @endforeach
                      
                    </tbody>
                  </table>
                  

                  <!-- /.table -->
                </div>
                <!-- /.mail-box-messages -->
              </div>
              <!-- /.card-body -->
              <div class="card-footer p-0">

              </div>
            </div>
            <!-- /.card -->
          </div>
      </div>
    </div>
</section>

@endsection

@push('styles')
<link rel="stylesheet" href="{{asset('/adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
@endpush

@push('scripts')
  <script src="{{asset('/adminlte/plugins/datatables/jquery.dataTables.js')}}"></script>
  <script src="{{asset('/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
  <script>
    $(function () {
      $("#example1").DataTable();
    });
  </script>
@endpush
