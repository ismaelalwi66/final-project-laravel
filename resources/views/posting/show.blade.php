@extends('layout/master')

@section('title')
    Details
@endsection

@section('content')
    
    <!-- Main content -->
    <section class="content pt-4">
        <div class="container-fluid">
          <div class="row">
            <!-- /.col -->
            <div class="col-md-9">                

                <!-- general form elements -->
                <div class="card card-primary">
                <!-- /.card -->
                    
                    <div class="card" style="margin-bottom: 0">
                        <div class="card-body">
                        <div class="tab-content">
                            <div class="active tab-pane" id="activity">
                            <!-- Post -->
                            <div class="post">
                                <div class="user-block">
                                
                                @if ($postingan->profile->image != "")
                                    <img class="img-circle img-bordered-sm" src="{{asset('profiles/images/'.$postingan->profile->image)}}" alt="user image">
                                @else
                                    <img class="img-circle img-bordered-sm" src="{{asset('profiles/images/user.png')}}" alt="user image">
                                @endif

                                
                                @if ($postingan->user_id == Auth::user()->id)
                                <div class="btn-group float-right">
                                    <div class="btn-group dropleft" role="group">
                                        <a href="#" class="btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-ellipsis-v"></i></a>
                                      <div class="dropdown-menu">    
                                       
                                        <button type="button" class="dropdown-item" data-toggle="modal" data-target="#exampleModal" data-id="{{$postingan->id}}" data-caption="{{$postingan->caption}}" data-image="{{$postingan->image}}">Edit</button>
                                        
                                        <form action="/posting/{{$postingan->id}}" method="POST">                                                        
                                            @csrf
                                            @method('DELETE')
                                        <button type="submit" class="dropdown-item">Delete</button>
                                        </form>
                                    </div>
                                    </div>
                                </div> 
                                @endif   
                                <span class="username">
                                    
                                @if (Auth::user()->id == $postingan->author->id)
                                    @if ($postingan->profile->name != "")
                                    <a href="/profile">{{$postingan->profile->name}}</a>
                                    @else
                                    <a href="/profile">{{$postingan->author->username}}</a>
                                    @endif
                                @else
                                    @if ($postingan->profile->name != "")
                                    <a href="/find-friend/{{$postingan->user_id}}">{{$postingan->profile->name}}</a>
                                    @else
                                    <a href="/find-friend/{{$postingan->user_id}}">{{$postingan->author->username}}</a>
                                    @endif
                                @endif    
                                    
                                    
                                </span>
                                <span class="description">{{ $postingan->created_at->diffForHumans() }}</span>
                                </div>
                                <!-- /.user-block -->
                                
                                <div class="row" mb-4>
                                    <div class="col-sm-12">  

                                        @if ($postingan->image != "")
                                            <img class="img-fluid" src="{{asset('postings/img/'.$postingan->image)}}">
                                        @endif                           
                                        
                                    </div>
                                </div>
                                <p>
                                {{$postingan->caption}}
                                </p>

                                
                            </div>                   
        
                            </div>
        
                        </div>
                        <!-- /.tab-content -->
                        </div><!-- /.card-body -->
                    </div>
                <!-- /.card -->
                </div>
                
                

            </div>

            <div class="col-md-3">
  
                <!-- Profile Image -->
                <div class="card card-primary ">
                  <div class="card-body box-profile">
                    <div class="text-center">
                       
                    </div>
                    

                    @php
                    $countcomment = App\Comment::where('commentable_id', $postingan->id)->get();

                    $likecount = App\Likepost::where('like_post_id', $postingan->id)->get();

                    @endphp

                    <form action="{{ route('like.store', $postingan->id) }}" method="post" class="d-inline-block">
                        @csrf
                        @if(Auth::user()->likes()->where('like_post_id', $postingan->id)->first())
                        <p>
                        <button style="color: blue;" type="submit" class="link-black btn-like-custom text-sm"><i class="far fa-thumbs-up mr-1"></i> <span>{{ count($likecount) }}</span> Unlike</button>
                        </p>
                        @else
                        <p>
                        <button type="submit" class="link-black btn-like-custom text-sm"><i class="far fa-thumbs-up mr-1"></i> <span>{{ count($likecount) }}</span> Like</button>
                        </p>
                        @endif
                    </form>
                    
                    <a class="w-100 collapsed" data-toggle="collapse" href="#collapseOne" aria-expanded="false">
                        <span class="float-right">
                            <span class="link-black text-sm">
                            <i class="far fa-comments mr-1"> {{ count($countcomment) }} Comments</i>
                            </span>
                        </span>
                    </a>             
                    <div id="accordion">
                        <div id="collapseOne" class="collapse" data-parent="#accordion" style="">
                            <div class="card-body">
                            @include('partials._comment_replies', ['comments' => $postingan->comments, 'post_id' => $postingan->id])
                            </div>
                        </div>
                    </div>                   

                    <form method="post" action="{{ route('comment.add') }}">
                        @csrf
                        <div class="input-group input-group-sm mb-0">
                            <input type="text" name="comment_body" class="form-control form-control-sm" placeholder="Type a comment" />
                            <input type="hidden" name="post_id" value="{{ $postingan->id }}" />
                        <div class="input-group-append">
                            <button type="submit" class="btn btn-danger">Send</button>
                        </div>
                        </div>
                    </form>
                    {{-- <h3 class="profile-username text-center">{{"@".$user->username}}</h3>
    
                    <p class="text-muted text-center">Software Engineer</p>
    
                    <ul class="list-group list-group-unbordered mb-3">
                      <li class="list-group-item">
                        <b>Followers</b> <a class="float-right"> {{count($follower)}} </a>
                      </li>
                      <li class="list-group-item">
                        <b>Following</b> <a class="float-right"> {{count($following)}} </a>
                      </li>
                    </ul> --}}
    
                  </div>
                  <!-- /.card-body -->
                </div>
                <!-- /.card -->
    
                <!-- /.card -->
              </div>
          <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
      <!-- /.content -->
      <!-- Modal -->


    <div class="modal fade " id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form action="" method="POST" enctype="multipart/form-data">
            @csrf
            @method('put')
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Posting</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <input name="caption" type="text" class="caption mb-3 form-control form-control-border border-width-2" id="exampleInputBorderWidth2" placeholder="What's happening ?">
                </div>
                <div class="form-group">

                    <label>Upload Image</label>
                    <div class="input-group">
                        <span class="input-group-btn mb-2">
                            <span class="btn btn-default btn-file">
                                Browse… <input name="image" type="file" id="imgInp">
                            </span>
                        </span>
                        <input type="text" class="img-upload form-control" readonly>
                    </div>
                    <img class="img-fluid rounded mx-auto d-block" id='img-upload'/>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Post</button>
            </div>
            </div>
        </form>
        </div>
    </div>

@endsection

@push('styles')

<style>
 .bg-comment {
    background-color: #f4f6f9;
    border-radius: 25px;
 }

 .img-box img {
    max-width: 30px;
    margin: 5px;
 }
 .row.display-comment .row.display-comment {
        margin-left: 40px
    }
.form-comment {
  margin-bottom: 5px;
}
.form-comment .input-group input {
    border-top-left-radius: 15px;
    border-bottom-left-radius: 15px;
}
.form-comment .input-group-append input {
    border-top-right-radius: 15px !important;
    border-bottom-right-radius: 15px !important;
}

.link-custom {
    display: inline;
    float: right;
    padding: 1px 5px;
    color: black;
}

.btn-like-custom {
  border: none;
  background: none;
}

</style>

@endpush

@push('scripts')
<script type="text/javascript">


    $('#exampleModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        
        var id = button.data('id')
        var caption = button.data('caption')
        var image = button.data('image')
        var action = '/posting/'+id

        var modal = $(this)
        modal.find('.modal-dialog form').attr('action',action)
        modal.find('.modal-body input.caption').val(caption)
        modal.find('.modal-body input.img-upload').val(image)
        modal.find('.form-group img').attr('src','{{asset('postings/img')}}/'+image)
      })

    $(document).ready( function() {
        $(document).on('change', '.btn-file :file', function() {
        var input = $(this),
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [label]);
        });

        $('.btn-file :file').on('fileselect', function(event, label) {
            
            var input = $(this).parents('.input-group').find(':text'),
                log = label;
            
            if( input.length ) {
                input.val(log);
            } else {
                if( log ) alert(log);
            }
        
        });
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                
                reader.onload = function (e) {
                    $('#img-upload').attr('src', e.target.result);
                }
                
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imgInp").change(function(){
            readURL(this);
        });     
    });

</script>
@endpush