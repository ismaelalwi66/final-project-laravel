@extends('layout/master')

@section('title')
    Home
@endsection

@section('content')
    
<section class="content" style="padding-top: 10px">
    <div class="container-fluid">
      <div class="row">
        <!-- /.col -->
        <div class="col-md-9">
            
            <form action="/posting" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="card card-primary">
                    <div class="card-header">
                    <h3 class="card-title">Different Styles</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                    <div class="form-group">
                        <label for="exampleInputBorderWidth2" style="text-transform: capitalize">Hi , {{$user->profile->name}} </label>
                        <input name="caption" type="text" class="form-control form-control-border border-width-2" id="exampleInputBorderWidth2" placeholder="What's happening ?">
                        @error('caption')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="row">
                        <div class="col-12">                            
                            <label for="files" class="btn btn-success">Add Image</label>
                            <input name="image" id="files" style="visibility:hidden;" type="file">    
                            @error('image')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror                          
                            <input type="submit" value="Post" class="btn btn-primary float-right">
                        </div>
                    </div>
                    <!-- /.card-body -->
                    </div>
                </div>
            </form>

@endsection