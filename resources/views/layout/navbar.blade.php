  <?php
    $profilefoto = App\Profile::where('user_id', Auth::user()->id)->first();
  ?>
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand-md navbar-light navbar-white">
    <div class="container">
      <a href="" class="navbar-brand">
        <img src="{{asset('adminlte/dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">
          JCC Media
        </span>
      </a>

      <button class="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse order-3" id="navbarCollapse">

        <ul class="navbar-nav">
          <li class="nav-item">
            <a href="/" class="nav-link">Beranda</a>
          </li>
          <li class="nav-item">
            <a href="{{ route('find-friend') }}" class="nav-link">Cari Teman</a>
          </li>
        </ul>
        
        <!-- SEARCH FORM -->
        {{-- <form class="form-inline ml-0 ml-md-3">
          <div class="input-group input-group-sm">
            <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
            <div class="input-group-append">
              <button class="btn btn-navbar" type="submit">
                <i class="fas fa-search"></i>
              </button>
            </div>
          </div>
        </form> --}}
        <form action="/find-friend" method="get">
          <div class="input-group input-group-sm">
            <input type="text" name="cari" class="form-control form-control-navbar" placeholder="Search People">
            <span class="input-group-append">
                <button type="submit" class="btn btn-navbar"><i class="fas fa-search"></i></button>
              </span>
          </div>
        </form> 
      </div>

      <div class="order-1 order-md-3 navbar-nav navbar-no-expand ml-auto">        
        <div class="btn-logout">
        <a href="{{ route('logout') }}" class="btn btn-primary" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="fas fa-sign-out-alt"></i> <b>{{ __('Logout') }}</b></a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              @csrf
          </form>
      </div>
      </div>
    </div>
  </nav>
  <!-- /.navbar -->