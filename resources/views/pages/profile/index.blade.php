@extends('layout.master')

@section('title')
    Profile Page
@endsection

@section('content')
<section class="content pt-4">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  @if($profile->image == '')
                  <img src="{{asset('/adminlte/dist/img/avatar3.png')}}" class="profile-user-img img-fluid img-circle" alt="User Image">
                  @else
                  <img class="profile-user-img img-fluid img-circle" src="{{asset('profiles/images/'.$profile->image)}}" alt="User profile picture">
                  @endif
                </div>

                <h3 class="profile-username text-center">
                  @if($profile->name == '')
                  {{ $profile->user->username }}
                  @else
                  {{ $profile->name }}
                  @endif
                </h3>

                <p class="text-muted text-center">{{ "@".$profile->user->username }}</p>

                <ul class="list-group list-group-unbordered mb-3">
                  <li class="list-group-item">
                      <b>Followers</b> <a class="float-right btn btn-primary btn-sm" href="{{ route('followers.followers', $profile->user->username) }}"> {{count($follower)}} </a>
                    </li>
                    <li class="list-group-item">
                      <b>Following</b> <a class="float-right btn btn-primary btn-sm" href="{{ route('following.following', $profile->user->username) }}"> {{count($following)}} </a>
                    </li>
                </ul>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <!-- About Me Box -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">About Me</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <strong><i class="fas fa-at mr-1"></i>Email</strong>

                <p class="text-muted">
                  {{ $profile->user->email }}
                </p>

                <hr>

                <strong><i class="fas fa-venus-mars mr-1"></i>Gender</strong>

                <p class="text-muted">
                  <span class="tag tag-danger">{{ $profile->gender }}, {{ $profile->age }} Tahun</span>
                </p>

                <hr>

                <strong><i class="fas fa-place-of-worship mr-1"></i>Hoby</strong>

                <p class="text-muted">
                  <span class="tag tag-danger">{{ $profile->hoby }}</span>
                </p>

                <hr>

                <strong><i class="fas fa-running mr-1"></i>Skill</strong>

                <p class="text-muted">
                  <span class="tag tag-danger">{{ $profile->skill }}</span>
                </p>

                <hr>

                <strong><i class="fas fa-map-marker-alt mr-1"></i>Location</strong>

                <p class="text-muted">
                  <span class="tag tag-danger">{{ $profile->location }}</span>
                </p>

                <button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#exampleModal" data-id="{{ $profile->user->id }}" data-username="{{ $profile->user->username }}" data-email="{{ $profile->user->email }}">Edit Account</button>
              </div>

              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
          <div class="col-md-9">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                  <li class="nav-item"><a class="nav-link active" href="#activity" data-toggle="tab">Activity</a></li>
                  <li class="nav-item"><a class="nav-link" href="#timeline" data-toggle="tab">Timeline</a></li>
                  <li class="nav-item"><a class="nav-link" href="#settings" data-toggle="tab">Bio</a></li>
                </ul>
              </div><!-- /.card-header -->

              <div class="card-body">
                <div class="tab-content">
                  <div class="tab-pane active" id="activity">
                    @foreach($posting as $key => $postitem)
                    <!-- Post -->
                    <div class="post">
                      <div class="user-block">
                        @if($postitem->profile->image == '')
                        <img src="{{asset('/adminlte/dist/img/avatar3.png')}}" class="img-circle img-bordered-sm" alt="User Image">
                        @else
                        <img class="img-circle img-bordered-sm" src="{{asset('profiles/images/'.$postitem->profile->image)}}" alt="user image">
                        @endif
                        <span class="username">
                          <a href="#">
                            @if($profile->name == '')
                            {{ $postitem->author->username }}
                            @else
                            {{ $postitem->profile->name }}
                            @endif
                          </a>
                          <a href="#" class="float-right btn-tool"><i class="fas fa-times"></i></a>
                        </span>
                        <span class="description">{{ $postitem->created_at->diffForHumans() }}</span>
                      </div>
                      <!-- /.user-block -->
                      <p>
                        {{ $postitem->caption }}
                      </p>

                      @php
                            $countcomment = App\Comment::where('commentable_id', $postitem->id)->get();

                            $likecount = App\Likepost::where('like_post_id', $postitem->id)->get();
                            @endphp
                          
                            <form action="{{ route('like.store', $postitem->id) }}" method="post" class="d-inline-block">
                              @csrf
                              @if(Auth::user()->likes()->where('like_post_id', $postitem->id)->first())
                              <p>
                                <button style="color: blue;" type="submit" class="link-black btn-like-custom text-sm"><i class="far fa-thumbs-up mr-1"></i> <span>{{ count($likecount) }}</span> Unlike</button>
                              </p>
                              @else
                              <p>
                                <button type="submit" class="link-black btn-like-custom text-sm"><i class="far fa-thumbs-up mr-1"></i> <span>{{ count($likecount) }}</span> Like</button>
                              </p>
                              @endif
                            </form>

                             <a class="w-100 collapsed" data-toggle="collapse" href="#collapseOne{{ $key + 1 }}" aria-expanded="false">
                                  <span class="float-right">
                                    <span class="link-black text-sm">
                                      <i class="far fa-comments mr-1"> {{ count($countcomment) }} Comments</i>
                                    </span>
                                  </span>
                                </a>
                                <div id="accordion">
                                  <div id="collapseOne{{ $key + 1 }}" class="collapse" data-parent="#accordion" style="">
                                    <div class="card-body">
                                      @include('partials._comment_replies', ['comments' => $postitem->comments, 'post_id' => $postitem->id])
                                    </div>
                                  </div>
                              </div>

                          <form method="post" action="{{ route('comment.add') }}">
                              @csrf
                              <div class="input-group input-group-sm mb-0">
                                <input type="text" name="comment_body" class="form-control form-control-sm" placeholder="Type a comment" />
                                <input type="hidden" name="post_id" value="{{ $postitem->id }}" />
                              <div class="input-group-append">
                                <button type="submit" class="btn btn-danger">Send</button>
                              </div>
                            </div>
                          </form>
                    </div>
                    <!-- /.post -->
                     @endforeach

                  </div>
                  <!-- /.tab-pane -->
                  <div class="tab-pane" id="timeline">
                    <!-- The timeline -->
                    <div class="timeline timeline-inverse">
                      <!-- timeline time label -->
                    
                      <!-- /.timeline-label -->
                      <div class="time-label">
                        <span class="bg-success">
                          Follow
                        </span>
                      </div>
                      <!-- timeline item -->
                       @foreach (Auth::user()->follows as $user)
                        <div>
                        <i class="fas fa-user bg-info"></i>

                        <div class="timeline-item">
                          <span class="time"><i class="far fa-clock"></i> {{ $user->pivot->created_at->diffForHumans() }}</span>

                          <h3 class="timeline-header border-0">You Following <a href="{{ route('friend-details', $user->id) }}">{{ $user->profile->name }}</a>
                          </h3>
                        </div>
                      </div>
                      @endforeach
                      <!-- END timeline item -->
                      <div class="time-label">
                        <span class="bg-red">
                          Like
                        </span>
                      </div>
                      <!-- timeline item -->
                      @foreach(Auth::user()->likes as $comment)
                      <div>
                        <i class="fas fa-heart bg-red"></i>

                        <div class="timeline-item">
                          <span class="time"><i class="far fa-clock"></i> {{ $comment->created_at->diffForHumans() }}</span>

                          <h3 class="timeline-header">You like Post <a href="{{ route('friend-details', $comment->profile->id) }}">{{ $comment->profile->name }}</a></h3>
                        </div>
                      </div>
                      @endforeach
                      <!-- END timeline item -->
                      <!-- timeline time label -->
                   
                      <!-- END timeline item -->
                      <div>
                        <i class="far fa-clock bg-gray"></i>
                      </div>
                    </div>
                  </div>
                  <!-- /.tab-pane -->

                  <div class="tab-pane" id="settings">
                    <form class="form-horizontal" action="{{ route('profile.update', $profile->id) }}" enctype="multipart/form-data" method="post">
                      @csrf
                      @method('put')
                      <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">Name</label>
                        <div class="col-sm-10">
                          <input name="name" type="text" class="form-control" id="inputName" placeholder="Name" name="name" value="{{ old('name', $profile->name) }}">
                          @error('name')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                          @enderror
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="gender" class="col-sm-2 col-form-label">Gender</label>
                        <div class="col-sm-10">
                          <?php
                            $val = Request::old('gender', $profile->gender)
                          ?>
                        <select class="form-control" name="gender">
                          <option value="" {{$val == ''?'selected':''}}>Pilih:</option>
                          <option value="Laki-Laki" {{$val == 'Laki-Laki'?'selected':''}}>Laki-Laki</option>
                          <option value="Wanita" {{$val == 'Wanita'?'selected':''}}>Wanita</option>
                        </select>
                        @error('gender')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                      </div>
                      </div>
                      <div class="form-group row">
                        <label for="bio" class="col-sm-2 col-form-label">Bio</label>
                        <div class="col-sm-10">
                          <textarea name="bio" class="form-control" id="summernote" placeholder="Bio">{!! old('bio', $profile->bio) !!}</textarea>
                          @error('bio')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                          @enderror
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="image" class="col-sm-2 col-form-label">Foto Profil</label>
                        <div class="col-sm-10">
                          <input type="file" class="form-control" id="image" placeholder="Foto Profil" name="image">
                          @error('image')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                          @enderror
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputskill" class="col-sm-2 col-form-label">Skill</label>
                        <div class="col-sm-10">
                          <input name="skill" type="text" class="form-control" id="inputskill" placeholder="Skill" name="skill" value="{{ old('skill', $profile->skill) }}">
                          @error('skill')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                          @enderror
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputHoby" class="col-sm-2 col-form-label">Hoby</label>
                        <div class="col-sm-10">
                          <input name="hoby" type="text" class="form-control" id="inputHoby" placeholder="Hoby" name="hoby" value="{{ old('hoby', $profile->hoby) }}">
                          @error('hoby')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                          @enderror
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputLocation" class="col-sm-2 col-form-label">Location</label>
                        <div class="col-sm-10">
                          <input name="location" type="text" class="form-control" id="inputLocation" placeholder="Location" name="location" value="{{ old('location', $profile->location) }}">
                          @error('location')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                          @enderror
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputage" class="col-sm-2 col-form-label">Name</label>
                        <div class="col-sm-10">
                          <input name="age" type="number" class="form-control" id="inputage" placeholder="age" name="age" value="{{ old('age', $profile->age) }}">
                          @error('age')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                          @enderror
                        </div>
                      </div>
                      <div class="form-group row">
                        <div class="offset-sm-2 col-sm-10">
                          <button type="submit" class="btn btn-danger">Submit</button>
                        </div>
                      </div>
                    </form>
                  </div>
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>

    <div class="modal fade " id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form action="" method="POST" enctype="multipart/form-data">
            @csrf
            @method('put')
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Account</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                  <label>Username</label>
                    <input name="username" type="text" class="username mb-1 form-control form-control-border border-width-2" id="exampleInputBorderWidth2" placeholder="Username ..." required>
                </div>
                <div class="form-group">
                  <label>Email</label>
                    <input name="email" type="email" class="email mb-1 form-control form-control-border border-width-2" id="exampleInputBorderWidth2" placeholder="Email ..." required>
                </div>
                <div class="form-group">
                  <label>Password</label>
                    <input name="password" type="password" class="password mb-1 form-control form-control-border border-width-2" id="exampleInputBorderWidth2" placeholder="Password ..">
                    <small>Kosongkan jika tidak ingin mengganti password</small>
                </div>
                <div class="form-group">
                  <label>Password Confirmation</label>
                    <input id="password-confirm" type="password" class="mb-1 form-control form-control-border border-width-2" name="password_confirmation" placeholder="Confirm Password" autocomplete="new-password">
                    <small>Kosongkan jika tidak ingin mengganti password</small>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
            </div>
        </form>
        </div>
    </div>

@endsection

@push('styles')

<!-- summernote -->
  <link rel="stylesheet" href="{{asset('adminlte/plugins/summernote/summernote-bs4.min.css')}}">

<style>
 .bg-comment {
    background-color: #f4f6f9;
    border-radius: 25px;
 }

 .img-box img {
    max-width: 30px;
    margin: 5px;
 }
 .row.display-comment .row.display-comment {
        margin-left: 40px
    }
.form-comment {
  margin-bottom: 5px;
}
.form-comment .input-group input {
    border-top-left-radius: 15px;
    border-bottom-left-radius: 15px;
}
.form-comment .input-group-append input {
    border-top-right-radius: 15px !important;
    border-bottom-right-radius: 15px !important;
}

.link-custom {
    display: inline;
    float: right;
    padding: 1px 5px;
    color: black;
}

.btn-like-custom {
  border: none;
  background: none;
}

</style>

@endpush

@push('scripts')
<!-- Summernote -->
<script src="{{asset('adminlte/plugins/summernote/summernote-bs4.min.js')}}"></script>
<script type="text/javascript">


    $('#exampleModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        
        var id = button.data('id')
        var username = button.data('username')
        var email = button.data('email')
        var action = '/profile/edit-account/'+id

        var modal = $(this)
        modal.find('.modal-dialog form').attr('action',action)
        modal.find('.modal-body input.username').val(username)
        modal.find('.modal-body input.email').val(email)
      });

    $(document).ready(function() {
      $('#summernote').summernote();
    });

</script>

@if(session('status-alert') == 'sukses')
<script>
  Swal.fire(
  'Account Berhasil di Update !',
  '',
  'success'
)
</script>
@endif

@if(session('status-alert') == 'sukses-bio')
<script>
  Swal.fire(
  'Bio Berhasil di Update !',
  '',
  'success'
)
</script>
@endif

@endpush


