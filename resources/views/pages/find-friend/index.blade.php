@extends('layout.master')


@section('title')
  Cari Teman
@endsection


@section('content')

<section class="content pt-4">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  @if($user->profile->image == '')
                  <img src="{{asset('/adminlte/dist/img/avatar3.png')}}" class="profile-user-img img-fluid img-circle" alt="User Image">
                  @else
                  <img class="profile-user-img img-fluid img-circle" src="{{asset('profiles/images/'.$user->profile->image)}}" alt="User profile picture">
                  @endif
                </div>

                @if($user->profile->name == '')
                <h3 class="profile-username text-center">{{"@".$user->username}}</h3>
                @else
                <h3 class="profile-username text-center">{{ $user->profile->name }}</h3>
                @endif

                  <p class="text-muted text-center">{{"@".$user->username }}</p>

                <ul class="list-group list-group-unbordered mb-3">
                  <li class="list-group-item">
                      <b>Followers</b> <a class="float-right btn btn-primary btn-sm" href="{{ route('followers.followers', $user->username) }}"> {{count($follower)}} </a>
                    </li>
                    <li class="list-group-item">
                      <b>Following</b> <a class="float-right btn btn-primary btn-sm" href="{{ route('following.following', $user->username) }}"> {{count($following)}} </a>
                    </li>
                </ul>

                <a href="{{ route('profile.index') }}" class="btn btn-primary btn-block">Lihat Profile</a>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <!-- About Me Box -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">About Me</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <strong><i class="fas fa-at mr-1"></i>Email</strong>

                <p class="text-muted">
                  {{ $user->email }}
                </p>

                <hr>

                <strong><i class="fas fa-venus-mars mr-1"></i>Gender</strong>

                <p class="text-muted">
                  <span class="tag tag-danger">{{ $user->profile->gender }}, {{ $user->profile->age }} Tahun</span>
                </p>

                <hr>

                <strong><i class="fas fa-place-of-worship mr-1"></i>Hoby</strong>

                <p class="text-muted">
                  <span class="tag tag-danger">{{ $user->profile->hoby }}</span>
                </p>

                <hr>

                <strong><i class="fas fa-running mr-1"></i>Skill</strong>

                <p class="text-muted">
                  <span class="tag tag-danger">{{ $user->profile->skill }}</span>
                </p>

                <hr>

                <strong><i class="fas fa-map-marker-alt mr-1"></i>Location</strong>

                <p class="text-muted">
                  <span class="tag tag-danger">{{ $user->profile->location }}</span>
                </p>

              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
    <div class="col-md-9">
          <div class="card card-primary card-outline">
            <!-- /.card-header -->
            <div class="card-body p-0">
              <div class="table-responsive mailbox-messages">
                <table class="table table-hover table-striped">
                  <tbody>
                  @foreach($profile as $item)
                  @if(Auth::user()->id != $item->id)
                  <tr>
                    <td class="align-middle">
                      <a href="{{ route('friend-details', $item->user->id) }}" class="user-block">
                        @if($item->image == '')
                        <img src="{{asset('profiles/images/user.png')}}" class="img-circle img-bordered-sm" alt="User Image">
                        @else
                        <img src="{{asset('profiles/images/'.$item->image)}}" class="img-circle img-bordered-sm" alt="User Image">
                        @endif
                      </a>
                    </td>
                    <td class="align-middle"><a href="{{ route('friend-details', $item->user->id) }}" class=""><b>
                      @if($item->name == '')
                      {{ $item->user->username }}
                      @else
                      {{ $item->name }}
                      @endif
                    </b></a></td>
                    <td class="align-middle">{{  $item->skill }}</td>
                    <td class="align-middle">
                      @if(Auth::user()->id != $item->id)
                      <form action="{{ route('following.store', $item) }}" method="post">
                        @csrf
                        @if(Auth::user()->follows()->where('following_user_id', $item->id)->first())
                        <button type="submit" class="btn btn-primary btn-block" value="Follow">Unfollow</button>
                        @else
                        <button type="submit" class="btn btn-primary btn-block" value="Follow">Follow</button>
                        @endif
                      </form>
                      @endif
                    </td>
                  </tr>
                  @endif
                  @endforeach
                  </tbody>
                </table>

                <!-- /.table -->
              </div>
              <!-- /.mail-box-messages -->
            </div>
            <!-- /.card-body -->
            <div class="card-footer p-0">

            </div>
          </div>
          <!-- /.card -->
        </div>
    </div>
    </div>
</section>

@endsection
