@extends('layout.master')

@section('title')
    Profile Page
@endsection

<!--https://www.youtube.com/watch?v=37-y-uXnM2Q&list=PLRKMmwY3-5MwADhthqRaewl-7e7AhjpP8&index=46-->
@section('content')
<section class="content pt-4">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  @if($user->profile->image == '')
                  <img src="{{asset('/adminlte/dist/img/avatar3.png')}}" class="profile-user-img img-fluid img-circle" alt="User Image">
                  @else
                  <img class="profile-user-img img-fluid img-circle" src="{{asset('profiles/images/'.$user->profile->image)}}" alt="User profile picture">
                  @endif
                </div>

                @if($user->profile->name == '')
                <h3 class="profile-username text-center">{{ $user->username}}</h3>
                @else
                <h3 class="profile-username text-center">{{ $user->profile->name }}</h3>
                @endif

                <p class="text-muted text-center">{{"@".$user->username }}</p>

                <ul class="list-group list-group-unbordered mb-3">
                  <li class="list-group-item">
                      <b>Followers</b> <a class="float-right btn btn-primary btn-sm" href="{{ route('followers.followers', $user->username) }}"> {{count($follower)}} </a>
                    </li>
                    <li class="list-group-item">
                      <b>Following</b> <a class="float-right btn btn-primary btn-sm" href="{{ route('following.following', $user->username) }}"> {{count($following)}} </a>
                    </li>
                </ul>
                @if(Auth::user()->id != $user->id)
                <form action="{{ route('following.store', $user) }}" method="post">
                  @csrf
                  @if(Auth::user()->follows()->where('following_user_id', $user->id)->first())
                  <button type="submit" class="btn btn-primary btn-block" value="Follow">Unfollow</button>
                  @else
                  <button type="submit" class="btn btn-primary btn-block" value="Follow">Follow</button>
                  @endif
                </form>
                @endif
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <!-- About Me Box -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">About Me</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <strong><i class="fas fa-at mr-1"></i>Email</strong>

                <p class="text-muted">
                  {{ $user->email }}
                </p>

                <hr>

                <strong><i class="fas fa-venus-mars mr-1"></i>Gender</strong>

                <p class="text-muted">
                  <span class="tag tag-danger">{{ $user->profile->gender }}, {{ $user->profile->age }} Tahun</span>
                </p>

                <hr>

                <strong><i class="fas fa-place-of-worship mr-1"></i>Hoby</strong>

                <p class="text-muted">
                  <span class="tag tag-danger">{{ $user->profile->hoby }}</span>
                </p>

                <hr>

                <strong><i class="fas fa-running mr-1"></i>Skill</strong>

                <p class="text-muted">
                  <span class="tag tag-danger">{{ $user->profile->skill }}</span>
                </p>

                <hr>

                <strong><i class="fas fa-map-marker-alt mr-1"></i>Location</strong>

                <p class="text-muted">
                  <span class="tag tag-danger">{{ $user->profile->location }}</span>
                </p>

              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
          <div class="col-md-9">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                  <li class="nav-item"><a class="nav-link active" href="#activity" data-toggle="tab">Activity</a></li>
                  <li class="nav-item"><a class="nav-link" href="#settings" data-toggle="tab">Bio</a></li>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                  <div class="tab-pane active" id="activity">

                    @foreach($posting as $key => $postitem)
                    <!-- Post -->
                    <div class="post">
                      <div class="user-block">
                        @if($postitem->profile->image == '')
                        <img src="{{asset('/adminlte/dist/img/avatar3.png')}}" class="img-circle img-bordered-sm" alt="User Image">
                        @else
                        <img class="img-circle img-bordered-sm" src="{{asset('profiles/images/'.$postitem->profile->image)}}" alt="user image">
                        @endif
                        <span class="username">
                          <a href="#">
                            @if($postitem->profile->name == '')
                            {{ $postitem->author->username }}
                            @else
                            {{ $postitem->profile->name }}
                            @endif
                          </a>
                          <a href="#" class="float-right btn-tool"><i class="fas fa-times"></i></a>
                        </span>
                        <span class="description">{{ $postitem->created_at->diffForHumans() }}</span>
                      </div>
                      <!-- /.user-block -->
                      <p>
                        {{ $postitem->caption }}
                      </p>

                      @php
                            $countcomment = App\Comment::where('commentable_id', $postitem->id)->get();

                            $likecount = App\Likepost::where('like_post_id', $postitem->id)->get();
                            @endphp
                          
                            <form action="{{ route('like.store', $postitem->id) }}" method="post" class="d-inline-block">
                              @csrf
                              @if(Auth::user()->likes()->where('like_post_id', $postitem->id)->first())
                              <p>
                                <button style="color: blue;" type="submit" class="link-black btn-like-custom text-sm"><i class="far fa-thumbs-up mr-1"></i> <span>{{ count($likecount) }}</span> Unlike</button>
                              </p>
                              @else
                              <p>
                                <button type="submit" class="link-black btn-like-custom text-sm"><i class="far fa-thumbs-up mr-1"></i> <span>{{ count($likecount) }}</span> Like</button>
                              </p>
                              @endif
                            </form>

                             <a class="w-100 collapsed" data-toggle="collapse" href="#collapseOne{{ $key + 1 }}" aria-expanded="false">
                                  <span class="float-right">
                                    <span class="link-black text-sm">
                                      <i class="far fa-comments mr-1"> {{ count($countcomment) }} Comments</i>
                                    </span>
                                  </span>
                                </a>
                                <div id="accordion">
                                  <div id="collapseOne{{ $key + 1 }}" class="collapse" data-parent="#accordion" style="">
                                    <div class="card-body">
                                      @include('partials._comment_replies', ['comments' => $postitem->comments, 'post_id' => $postitem->id])
                                    </div>
                                  </div>
                              </div>

                          <form method="post" action="{{ route('comment.add') }}">
                              @csrf
                              <div class="input-group input-group-sm mb-0">
                                <input type="text" name="comment_body" class="form-control form-control-sm" placeholder="Type a comment" />
                                <input type="hidden" name="post_id" value="{{ $postitem->id }}" />
                              <div class="input-group-append">
                                <button type="submit" class="btn btn-danger">Send</button>
                              </div>
                            </div>
                          </form>
                    </div>
                    <!-- /.post -->
                     @endforeach

                  </div>
                  <!-- /.tab-pane -->
          
                  <!-- /.tab-pane -->

                  <div class="tab-pane" id="settings">
                      <div class="form-group row">
                        <label for="bio" class="col-sm-2 col-form-label">Bio</label>
                        <div class="col-sm-10">
                          <p>{!!$user->profile->bio!!}</p>
                        </div>
                      </div>
                  </div>
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
@endsection

@push('styles')

<style>
 .bg-comment {
    background-color: #f4f6f9;
    border-radius: 25px;
 }

 .img-box img {
    max-width: 30px;
    margin: 5px;
 }
 .row.display-comment .row.display-comment {
        margin-left: 40px
    }
.form-comment {
  margin-bottom: 5px;
}
.form-comment .input-group input {
    border-top-left-radius: 15px;
    border-bottom-left-radius: 15px;
}
.form-comment .input-group-append input {
    border-top-right-radius: 15px !important;
    border-bottom-right-radius: 15px !important;
}

.link-custom {
    display: inline;
    float: right;
    padding: 1px 5px;
    color: black;
}

.btn-like-custom {
  border: none;
  background: none;
}

</style>

@endpush
