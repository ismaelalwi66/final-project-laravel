@extends('layout.master')

@section('title')
    Home
@endsection

@section('content')
    <!-- Main content -->
    <section class="content pt-4">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-3">

              <!-- Profile Image -->
              <div class="card card-primary card-outline">
                <div class="card-body box-profile">
                  <div class="text-center">
                    @if($user->profile->image == '')
                    <img src="{{asset('/profiles/images/user.png')}}" class="profile-user-img img-fluid img-circle" alt="User Image">
                    @else
                    <img class="profile-user-img img-fluid img-circle"
                         src="{{asset('profiles/images/'.$user->profile->image)}}"
                         alt="User profile picture">
                    @endif
                  </div>

                  @if($user->profile->name == '')
                  <h3 class="profile-username text-center">{{"@".$user->username}}</h3>
                  @else
                  <h3 class="profile-username text-center">{{ $user->profile->name }}</h3>
                  @endif

                  <p class="text-muted text-center">{{"@".$user->username }}</p>

                  <ul class="list-group list-group-unbordered mb-3">
                    <li class="list-group-item">
                      <b>Followers</b> <a class="float-right btn btn-primary btn-sm" href="{{ route('followers.followers', $user->username) }}"> {{count($follower)}} </a>
                    </li>
                    <li class="list-group-item">
                      <b>Following</b> <a class="float-right btn btn-primary btn-sm" href="{{ route('following.following', $user->username) }}"> {{count($following)}} </a>
                    </li>
                  </ul>

                  <a href="{{ route('profile.index') }}" class="btn btn-primary btn-block">Lihat Profile</a>

                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->

              <!-- /.card -->
            </div>
            <!-- /.col -->
            <div class="col-md-6">
                <form action="/posting" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="card card-primary">
                        <div class="card-header">
                        <h3 class="card-title">Post</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                        <div class="form-group">
                            <label for="exampleInputBorderWidth2" style="text-transform: capitalize">Hi ,  {{ $user->profile->name }}</label>
                            <input name="caption" type="text" class="form-control form-control-border border-width-2" id="exampleInputBorderWidth2" placeholder="What's happening ?">
                            @error('caption')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <label for="files" class="btn btn-success">Add Image</label>
                                <input name="image" id="files" style="visibility:hidden;" type="file">
                                @error('image')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                                <input type="submit" value="Post" class="btn btn-primary float-right">
                            </div>
                        </div>
                        <!-- /.card-body -->
                        </div>
                    </div>
                </form>
                @foreach($posting as $key => $postingan)
                @if ($postingan->user_id == $id)
                <div class="card">
                    <div class="card-body">
                        <div class="post">
                          <div class="user-block">
                            @if($postingan->profile->image == '')
                            <img class="img-circle img-bordered-sm" src="{{ asset('profiles/images/user.png') }}" alt="user image">
                            @else
                            <img class="img-circle img-bordered-sm" src="{{ asset('profiles/images/'.$postingan->profile->image) }}" alt="user image">
                            @endif

                            <div class="btn-group float-right">
                                <div class="btn-group dropleft" role="group">
                                    <a href="#" class="btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-ellipsis-v"></i></a>
                                  <div class="dropdown-menu">

                                    <a class="dropdown-item" href="/posting/{{$postingan->id}}">Details</a>
                                    <button type="button" class="dropdown-item" data-toggle="modal" data-target="#exampleModal" data-id="{{$postingan->id}}" data-caption="{{$postingan->caption}}" data-image="{{$postingan->image}}">Edit</button>

                                    <form action="/posting/{{$postingan->id}}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                    <button type="submit" class="dropdown-item">Delete</button>
                                    </form>
                                </div>
                                </div>
                            </div>

                            <span class="username">
                              <a href="{{ route('friend-details', $postingan->author->id) }}">
                                @if($postingan->profile->name == '')
                                {{ $postingan->author->username }}
                                @else
                                {{ $postingan->profile->name }}
                                @endif
                              </a>
                            </span>
                            <span class="description">{{ $postingan->created_at->diffForHumans() }}</span>
                          </div>
                          <!-- /.user-block -->
                          <div class="row mb-2">
                            <div class="col-sm-12">

                                @if ($postingan->image != "")
                                    <img class="img-fluid" src="{{asset('postings/img/'.$postingan->image)}}">
                                @endif

                            </div>
                        </div>
                          <p>
                            {{ $postingan->caption }}
                          </p>
                            @php
                            $countcomment = App\Comment::where('commentable_id', $postingan->id)->get();

                            $likecount = App\Likepost::where('like_post_id', $postingan->id)->get();

                            @endphp

                            <form action="{{ route('like.store', $postingan->id) }}" method="post" class="d-inline-block">
                              @csrf
                              @if(Auth::user()->likes()->where('like_post_id', $postingan->id)->first())
                              <p>
                                <button style="color: blue;" type="submit" class="link-black btn-like-custom text-sm"><i class="far fa-thumbs-up mr-1"></i> <span>{{ count($likecount) }}</span> Unlike</button>
                              </p>
                              @else
                              <p>
                                <button type="submit" class="link-black btn-like-custom text-sm"><i class="far fa-thumbs-up mr-1"></i> <span>{{ count($likecount) }}</span> Like</button>
                              </p>
                              @endif
                            </form>

                              <a class="w-100 collapsed" data-toggle="collapse" href="#collapseOne{{ $key + 1 }}" aria-expanded="false">
                                  <span class="float-right">
                                    <span class="link-black text-sm">
                                      <i class="far fa-comments mr-1"> {{ count($countcomment) }} Comments</i>
                                    </span>
                                  </span>
                              </a>
                                <div id="accordion">
                                  <div id="collapseOne{{ $key + 1 }}" class="collapse" data-parent="#accordion" style="">
                                    <div class="card-body">
                                      @include('partials._comment_replies', ['comments' => $postingan->comments, 'post_id' => $postingan->id])
                                    </div>
                                  </div>
                              </div>

                          <form method="post" action="{{ route('comment.add') }}">
                              @csrf
                              <div class="input-group input-group-sm mb-0">
                                <input type="text" name="comment_body" class="form-control form-control-sm" placeholder="Type a comment" />
                                <input type="hidden" name="post_id" value="{{ $postingan->id }}" />
                              <div class="input-group-append">
                                <button type="submit" class="btn btn-danger">Send</button>
                              </div>
                            </div>
                          </form>

                        </div>
                    </div>
                </div>
                @endif
                @foreach ($following as $followings)
                    @if ($postingan->user_id == $followings->following_user_id)
                    <div class="card">
                    <div class="card-body">
                        <div class="post">
                          <div class="user-block">
                            @if($postingan->profile->image == '')
                            <img class="img-circle img-bordered-sm" src="{{ asset('profiles/images/user.png') }}" alt="user image">
                            @else
                            <img class="img-circle img-bordered-sm" src="{{ asset('profiles/images/'.$postingan->profile->image) }}" alt="user image">
                            @endif
                            <div class="btn-group float-right">
                                <div class="btn-group dropleft" role="group">
                                    <a href="#" class="btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-ellipsis-v"></i></a>
                                  <div class="dropdown-menu">

                                    <a class="dropdown-item" href="/posting/{{$postingan->id}}">Details</a>

                                </div>
                                </div>
                            </div>

                            <span class="username">
                              <a href="{{ route('friend-details', $postingan->author->id) }}">
                                @if($postingan->profile->name == '')
                                {{ $postingan->author->username }}
                                @else
                                {{ $postingan->profile->name }}
                                @endif
                              </a>
                            </span>
                            <span class="description">{{ $postingan->created_at->diffForHumans() }}</span>
                          </div>
                          <!-- /.user-block -->
                          <div class="row mb-2">
                            <div class="col-sm-12">

                                @if ($postingan->image != "")
                                    <img class="img-fluid" src="{{asset('postings/img/'.$postingan->image)}}">
                                @endif

                            </div>
                        </div>
                          <p>
                            {{ $postingan->caption }}
                          </p>
                            @php
                            $countcomment = App\Comment::where('commentable_id', $postingan->id)->get();

                            $likecount = App\Likepost::where('like_post_id', $postingan->id)->get();

                            @endphp

                            <form action="{{ route('like.store', $postingan->id) }}" method="post" class="d-inline-block">
                              @csrf
                              @if(Auth::user()->likes()->where('like_post_id', $postingan->id)->first())
                              <p>
                                <button style="color: blue;" type="submit" class="link-black btn-like-custom text-sm"><i class="far fa-thumbs-up mr-1"></i> <span>{{ count($likecount) }}</span> Unlike</button>
                              </p>
                              @else
                              <p>
                                <button type="submit" class="link-black btn-like-custom text-sm"><i class="far fa-thumbs-up mr-1"></i> <span>{{ count($likecount) }}</span> Like</button>
                              </p>
                              @endif
                            </form>

                            <a class="w-100 collapsed" data-toggle="collapse" href="#collapseOne{{ $key + 1 }}" aria-expanded="false">
                                  <span class="float-right">
                                    <span class="link-black text-sm">
                                      <i class="far fa-comments mr-1"> {{ count($countcomment) }} Comments</i>
                                    </span>
                                  </span>
                            </a>
                            <div id="accordion">
                                <div id="collapseOne{{ $key + 1 }}" class="collapse" data-parent="#accordion" style="">
                                  <div class="card-body">
                                    @include('partials._comment_replies', ['comments' => $postingan->comments, 'post_id' => $postingan->id])
                                  </div>
                                </div>
                            </div>

                          <form method="post" action="{{ route('comment.add') }}">
                              @csrf
                              <div class="input-group input-group-sm mb-0">
                                <input type="text" name="comment_body" class="form-control form-control-sm" placeholder="Type a comment" required />
                                <input type="hidden" name="post_id" value="{{ $postingan->id }}" />
                              <div class="input-group-append">
                                <button type="submit" class="btn btn-danger">Send</button>
                              </div>
                            </div>
                          </form>

                        </div>
                    </div>
                </div>
                    @endif
                @endforeach

            @endforeach
            <!-- /.col -->
            </div>

            <div class="col-md-3">

              <!-- Profile Image -->
              <div class="card">
              <div class="card-header">
                <h3 class="card-title">Sugest Friend</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0">
                <ul class="nav nav-pills flex-column">
                  @foreach($sugestuser as $usersugest)

                  <li class="nav-item">
                    <a class="nav-link" href="{{ route('friend-details', $usersugest->id) }}">
                      @if($usersugest->profile->image == '')
                        <img src="{{asset('/profiles/images/user.png')}}" class="profile-user-img img-fluid img-circle custom-image mr-1" alt="User Image">
                        @else
                        <img class="profile-user-img img-fluid img-circle custom-image mr-1"
                             src="{{asset('profiles/images/'.$usersugest->profile->image)}}"
                             alt="User profile picture">
                      @endif
                      <span><b>
                        @if($usersugest->profile->name == '')
                        {{ $usersugest->username }}
                        @else
                        {{ $usersugest->profile->name }}
                        @endif
                      </b></span>
                    </a>
                  </li>
                  @endforeach
                </ul>
              </div>
              <!-- /.card-body -->
            </div>
              <!-- /.card -->

              <!-- /.card -->
            </div>
          <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
      <!-- /.content -->


      <!-- Modal -->


    <div class="modal fade " id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form action="" method="POST" enctype="multipart/form-data">
            @csrf
            @method('put')
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Posting</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <input name="caption" type="text" class="caption mb-3 form-control form-control-border border-width-2" id="exampleInputBorderWidth2" placeholder="What's happening ?">
                </div>
                <div class="form-group">

                    <label>Upload Image</label>
                    <div class="input-group">
                        <span class="input-group-btn mb-2">
                            <span class="btn btn-default btn-file">
                                Browse… <input name="image" type="file" id="imgInp">
                            </span>
                        </span>
                        <input type="text" class="img-upload form-control" readonly>
                    </div>
                
                    <img class="img-fluid rounded mx-auto d-block" id='img-upload'/>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Post</button>
            </div>
            </div>
        </form>
        </div>
    </div>

@endsection

@push('styles')

<style>
 .bg-comment {
    background-color: #f4f6f9;
    border-radius: 25px;
 }

 .img-box img {
    max-width: 30px;
    margin: 5px;
 }
 .row.display-comment .row.display-comment {
        margin-left: 40px
    }
.form-comment {
  margin-bottom: 5px;
}
.form-comment .input-group input {
    border-top-left-radius: 15px;
    border-bottom-left-radius: 15px;
}
.form-comment .input-group-append input {
    border-top-right-radius: 15px !important;
    border-bottom-right-radius: 15px !important;
}

.link-custom {
    display: inline;
    float: right;
    padding: 1px 5px;
    color: black;
}

.btn-like-custom {
  border: none;
  background: none;
}

.custom-image {
  height: 40px;
  width: 40px;
}

</style>

@endpush

@push('scripts')

<script type="text/javascript">


    $('#exampleModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal

        var id = button.data('id')
        var caption = button.data('caption')
        var image = button.data('image')
        var action = '/posting/'+id

        var modal = $(this)
        modal.find('.modal-dialog form').attr('action',action)
        modal.find('.modal-body input.caption').val(caption)
        modal.find('.modal-body input.img-upload').val(image)
        modal.find('.form-group img').attr('src','{{asset('postings/img')}}/'+image)
      })

    $(document).ready( function() {
        $(document).on('change', '.btn-file :file', function() {
        var input = $(this),
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [label]);
        });

        $('.btn-file :file').on('fileselect', function(event, label) {

            var input = $(this).parents('.input-group').find(':text'),
                log = label;

            if( input.length ) {
                input.val(log);
            } else {
                if( log ) alert(log);
            }

        });
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#img-upload').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imgInp").change(function(){
            readURL(this);
        });
    });

</script>

@if(session('status-alert') == 'sukses')
<script>
  Swal.fire(
  'Postingan berhasil di Upload !',
  '',
  'success'
)
</script>
@endif

@if(session('status-alert') == 'gagal-posting')
<script>
  Swal.fire(
  'Gagal untuk memposting !',
  '',
  'error'
)
</script>
@endif

@if(session('status-alert') == 'sukses-dihapus')
<script>
  Swal.fire(
  'Postingan berhasil di Hapus !',
  '',
  'success'
)
</script>
@endif

@if(session('status-alert') == 'sukses-diupdate')
<script>
  Swal.fire(
  'Postingan berhasil di Update !',
  '',
  'success'
)
</script>
@endif

@if(session('status-alert') == 'sukses-update-komen')
<script>
  Swal.fire(
  'Komen berhasil di Update !',
  '',
  'success'
)
</script>
@endif

@if(session('status-alert') == 'sukses-hapus-komen')
<script>
  Swal.fire(
  'Komen berhasil di hapus !',
  '',
  'success'
)
</script>
@endif


@endpush

