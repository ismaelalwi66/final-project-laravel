@foreach($comments as $key => $comment)
<div class="row">
   <div class="img-box">
        @if($comment->profile->image == '')
        <img class="img-circle img-bordered-sm" src="{{ asset('profiles/images/user.png') }}" alt="user image">
        @else
        <img class="img-circle img-bordered-sm" src="{{ asset('profiles/images/'.$comment->profile->image) }}" alt="user image">
        @endif
    </div>
    <div class="display-comment px-3 py-2 d-inline-block mb-2 bg-comment">
      <div class="text-box">
        <a href="{{ route('friend-details', $comment->user->id) }}"><strong>
          @if($comment->profile->name == '')
          {{ $comment->user->username }}
          @else
          {{ $comment->profile->name }}
          @endif
      </strong></a>
        @if(Auth::user()->id == $comment->user->id)
        <a id="dropdowncomment" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link link-custom dropdown-toggle"></a>
        <ul aria-labelledby="dropdowncomment" class="dropdown-menu border-0 shadow" style="left: 0px; right: inherit;">
              <li><button data-toggle="modal" data-target="#exampleModal{{ $comment->id }}"  class="dropdown-item">Edit</button></li>
              <li>
                <form action="{{ route('comment.delete', $comment->id) }}" method="POST">
                    @method('delete')
                    @csrf
                    <input type="submit" class="dropdown-item" value="Delete">
                </form>
              </li>
            </ul>
          @endif
        <small class="d-block">{{ $comment->created_at->diffForHumans() }}</small>
        <p>{{ $comment->body }}</p>
        <a href="" id="reply"></a>
        <form class="form-comment" method="post" action="{{ route('reply.add') }}">
            @csrf
            <div class="input-group input-group-sm mb-0">
                <input type="text" name="comment_body" class="form-control form-control-sm" placeholder="Reply ..." required />
                <input type="hidden" name="post_id" value="{{ $post_id }}" />
                <input type="hidden" name="comment_id" value="{{ $comment->id }}" />
            <div class="input-group-append">
                <input type="submit" class="btn btn-warning" value="Reply" />
            </div>
        </div>
        </form>
      </div>
      @php
        $likecomcount = App\Likecomment::where('like_comment_id', $comment->id)->get();
      @endphp
      <form action="{{ route('likecomment.store', $comment->id) }}" method="post" class="d-inline-block">
      @csrf
      @if(Auth::user()->likescomment()->where('like_comment_id', $comment->id)->first())
      <p>
        <button style="color: blue;" type="submit" class="link-black btn-like-custom text-sm"><i class="far fa-thumbs-up mr-1"></i> <span>{{ count($likecomcount) }}</span> Unlike</button>
      </p>
      @else
      <p>
        <button type="submit" class="link-black btn-like-custom text-sm"><i class="far fa-thumbs-up mr-1"></i> <span>{{ count($likecomcount) }}</span> Like</button>
      </p>
      @endif
    </form>
      
    </div>
  </div>
  <div class="fancy ml-3 mb-2">
    @include('partials._comment_replies', ['comments' => $comment->replies])
  </div>

  <!-- Modal -->
    <div class="modal fade" id="exampleModal{{ $comment->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Edit Comment</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="{{ route('comment.update', $comment->id) }}" method="post">
            @csrf
            @method('put')
            <div class="form-group">
              <label for="recipient-name" class="col-form-label">Your Comment :</label>
              <input type="text" class="form-control" name="commentnew" value="{{ $comment->body }} ">
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Update</button>
        </div>
      </form>
      </div>
    </div>
  </div>

@endforeach
