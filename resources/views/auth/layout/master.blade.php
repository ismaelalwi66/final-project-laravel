<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>
    <style>
        body{
            background-image: url(pglogin/images/bg4.jpg);  background-repeat: no-repeat;
            background-size: cover;
        }
        .login-box{
            box-shadow:2px 2px 6px #000;
        }
        .register-box{
            box-shadow:12px 12px 20px #0029;
        }
        .card-body{
            border-radius: 50px;
        }
        .register-box, .card-body{
            border-radius: 50px;
        }
        .reg{
            font-size: 12pt; text-align: center;
        }
        a.text-center{
            text-decoration: none; color:#3c8dbc;
        }
        button.btn{
            border-radius: 50px;
        }
        input.form-control{
            border-radius: 50px;
        }
        p.label{
            font-size: 10pt; text-align: right;
        }
        </style>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('adminlte/plugins/fontawesome-free/css/all.min.css')}}">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="{{asset('adminlte/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('adminlte/dist/css/adminlte.min.css')}}">
</head>
<body class="hold-transition login-page" >

@yield('content')


<!-- jQuery -->
<script src="{{asset('adminlte/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('adminlte/dist/js/adminlte.min.js')}}"></script>
</body>
</html>