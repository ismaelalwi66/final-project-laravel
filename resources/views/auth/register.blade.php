@extends('auth.layout.master')
@section('title')
    Register Page
@endsection

@section('content')
<div class="register-box">
    <div class="card card-outline card-primary">
      <div class="text-center mt-3">
        <a class="h1"><b>Register</b></a>
      </div>
      <div class="card-body">
        <p class="login-box-msg">{{ __('Register a new account') }}</p>
  
        <form action="{{ route('register') }}" method="POST">
            @csrf
          <div class="form-group">
            {{-- <label for="username" class="col-md-4 text-md-left">{{ __('Username') }}</label> --}}
            <div class="col-md-12">
                <input id="username" type="text" class="form-control @error('username') is-invalid @enderror" name="username" placeholder="Username" value="{{ old('username') }}">

                @error('username')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
          <div class="form-group">
            {{-- <label for="email" class="col-md-4 text-sm-left">{{ __('E-Mail') }}</label> --}}

            <div class="col-md-12">
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" placeholder="E-mail" value="{{ old('email') }}" required autocomplete="email">

                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
          <div class="form-group">
            {{-- <label for="password" class="col-md-4 text-md-left">{{ __('Password') }}</label> --}}

            <div class="col-md-12">
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Password" required autocomplete="new-password">

                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
          <div class="form-group">
            {{-- <label for="password-confirm" class="col-md-6 text-md-left">{{ __('Confirm Password') }}</label> --}}

            <div class="col-md-12">
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password" required autocomplete="new-password">
            </div>
        </div>
          <div class="row">
            </div> 
            <div class="col my-2">
              <button type="submit" class="btn btn-primary btn-block">{{ __('Register') }}</button>
            </div>
          </div>
        </form>
  
  
        <p class="reg my-3"><a href="login" class="text-center">I already have an account</a></p>
      </div>
    </div>
  </div>
@endsection
