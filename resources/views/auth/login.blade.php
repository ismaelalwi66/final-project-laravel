@extends('auth.layout.master')
@section('title')
    Login Page
@endsection

@section('content')
    <div class="login-box">
    <div class="card card-outline card-primary" >
  <div class="text-center mt-4">
    <a class="h1"><b>{{ __('Login') }}</b></a>
  </div>
  <!-- /.login-logo -->
    <div class="card-body login-card-body">
      <p class="login-box-msg">Share your feels here</p>

      <form method="POST" action="{{ route('login') }}">
        @csrf
        <div class="col-md-12">
            {{-- <label for="username" class="col-md-6 col-form-label text-md-left">{{ __('Username / E-Mail') }}</label> --}}
            <input id="username" type="username" class="form-control my-3 @error('username') is-invalid @enderror" name="username" placeholder="Username / E-mail" value="{{ old('username') }}" required autocomplete="username" autofocus>

            @error('username')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
        <div class="col-md-12">
            {{-- <label for="password" class="col-md-6 col-form-label text-md-left">{{ __('Password') }}</label> --}}
            <input id="password" type="password" class="form-control my-3 @error('password') is-invalid @enderror" name="password" placeholder="Password" required autocomplete="current-password">

            @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
        <div class="col">
            <br><button type="submit" class="btn btn-primary btn-block" >{{ __('Login') }}</button>
        </div>
      </form>      
      <p class="reg mt-5 text-center">
        <a href="register" >Register</a>
      </p>
    </div>
  </div>
</div>
@endsection
