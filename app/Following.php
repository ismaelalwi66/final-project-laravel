<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Following extends Model
{
    protected $table = 'following';

    public function follow()
    {
    return $this->belongsToMany('App\Users');
    }

    public function follower()
    {
    return $this->belongsToMany('App\Profile');
    }

    public function profile()
    {
        return $this->belongsTo('App\Profile', 'following_user_id');
    }

}
