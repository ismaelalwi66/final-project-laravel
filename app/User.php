<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

use App\Posting;
use App\Comment;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function profile(){
        return $this->hasOne('App\Profile');
    }

    public function comment()
    {
        return $this->hasMany('App\Comment', 'user_id');
    }

    public function follows()
    {
        return $this->belongsToMany(User::class, 'following', 'user_id', 'following_user_id')->withTimestamps();
    }

    public function followers() 
    {
        return $this->belongsToMany(User::class, 'following', 'user_id', 'following_user_id')->withTimestamps();
    }

    public function follow(User $user)
    {
        return $this->follows()->save($user);
    }

    public function unfollow(User $user)
    {
        return $this->follows()->detach($user);
    }

    public function hasFollow(User $user)
    {
        return $this->follows()->where('following_user_id', $user->id)->exists();
    }

    public function likes()
    {
        return $this->belongsToMany(Posting::class, 'likeposts', 'user_id', 'like_post_id')->withTimestamps();
    }

    public function like(Posting $post)
    {
        return $this->likes()->save($post);
    }

    public function unlike(Posting $post)
    {
        return $this->likes()->detach($post);
    }

    public function hasLike(Posting $post)
    {
        return $this->likes()->where('like_post_id', $post->id)->exists();
    }

    public function likescomment()
    {
        return $this->belongsToMany(Comment::class, 'likecomments', 'user_id', 'like_comment_id')->withTimestamps();
    }

    public function likecomment(Comment $comment)
    {
        return $this->likescomment()->save($comment);
    }

    public function unlikecomment(Comment $comment)
    {
        return $this->likescomment()->detach($comment);
    }

    public function hasLikecomment(Comment $comment)
    {
        return $this->likescomment()->where('like_comment_id', $comment->id)->exists();
    }
    
}
