<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = 'profile';
    protected $fillable = ['name', 'gender', 'bio', 'image', 'skill', 'hoby', 'location', 'age', 'user_id'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
