<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Posting extends Model
{
    protected $table = "posting";
    protected $guarded = [];

    public function author(){
        return $this->belongsTo('App\User','user_id');
    }

    public function profile()
    {
        return $this->belongsTo('App\Profile', 'user_id', 'id');
    }

    public function comments()
    {
        return $this->morphMany('App\Comment', 'commentable')->whereNull('parent_id');
    }
}
