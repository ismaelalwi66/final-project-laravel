<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profile;
use App\Posting;
use App\User;
use App\Following;
use Auth;
use File;
use Validator;
class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $profile = Profile::where('user_id', Auth::user()->id)->first();
        $posting = Posting::where('user_id', Auth::user()->id)->get();
        $following = Following::where('user_id',Auth::user()->id)->get();
        $follower = Following::where('following_user_id',Auth::user()->id)->get();
        $gender = Profile::all();

        return view('pages.profile.index', compact('profile', 'posting', 'gender', 'following', 'follower'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'max:255',
            'gender' => 'in:Laki-Laki,Wanita',
            'image' => 'mimes:jpg,jpeg,png|max:2200',
            'skill' => 'max:100',
            'hoby' => 'max:100',
            'location' => 'max:225',
            'age' => 'digits_between:1, 3'
        ]);

        $profile = Profile::findOrFail($id);

        if ($request->has('image')) {
            $path = 'profiles/images/';
            File::delete($path . $profile->image);
            $gambar = $request->image;
            $new_gambar = time() . ' - ' . $gambar->getClientOriginalName();
            $gambar->move($path, $new_gambar);
            $profile_data = [
                'name' => $request->name,
                'gender' => $request->gender,
                'bio' => $request->bio,
                'image' => $new_gambar,
                'skill' => $request->skill,
                'hoby' => $request->hoby,
                'location' => $request->location,
                'age' => $request->age,
            ];
        } else {
            $profile_data = [
                'name' => $request->name,
                'gender' => $request->gender,
                'bio' => $request->bio,
            ];
        }

        $profile->update($profile_data);

        return redirect()->route('profile.index')->with('status-alert', 'sukses-bio');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function findfriend(Request $request)
    {
        $cari = $request->cari;
        $user = User::where('id', Auth::user()->id)->first();
        $profile = Profile::where('name', 'like', "%$cari%")->orwhere('gender', 'like', "%$cari%")->get();
        $following = Following::where('user_id',Auth::user()->id)->get();
        $follower = Following::where('following_user_id',Auth::user()->id)->get();

        return view('pages.find-friend.index', compact('profile', 'cari', 'user', 'following', 'follower'));
    }

    public function findfrienddetails($id)
    {
        $user = User::where('id', $id)->firstOrFail();
        $posting = Posting::where('user_id', $id)->get();
        $following = Following::where('user_id',$id)->get();
        $follower = Following::where('following_user_id',$id)->get();

        return view('pages.find-friend.friend-details', compact('user', 'posting', 'following', 'follower'));
    }
}
