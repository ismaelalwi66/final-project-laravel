<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Profile;
use App\Following;

class FollowingController extends Controller
{
 
    public function store(Request $request, User $user)
    {
        if (Auth::user()->hasFollow($user)) {

            Auth::user()->unfollow($user);

        } else {

            Auth::user()->follow($user);

        }

        return back()->with("Success", "You Are Follow User");
    }

    public function followers($username){

        $data = Profile::all();
        $user = User::where('username',$username)->first();
        $id = $user -> id;        
        $profile = Profile::where('id',$id)->first();
        $follower = following::where('following_user_id',$id)->get();
        $following = following::where('user_id',$id)->get();

        return view('follow.followers',compact('follower','following','user','data','profile'));
    }

    public function following($username){

        $data = Profile::all();
        $user = User::where('username',$username)->first();
        $id = $user -> id;
        $profile = Profile::where('id',$id)->first();
        $following = following::where('user_id',$id)->get();        
        $follower = following::where('following_user_id',$id)->get();


        return view('follow.following',compact('following','follower','user','data','profile'));
    }


}
