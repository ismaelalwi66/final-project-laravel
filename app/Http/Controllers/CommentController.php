<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use App\Posting;

class CommentController extends Controller
{
    public function store(Request $request)
    {
        $comment = new Comment;
        $comment->body = $request->get('comment_body');
        $comment->user()->associate($request->user());
        $post = Posting::find($request->get('post_id'));
        $post->comments()->save($comment);

        return back();
    }

    public function replyStore(Request $request)
    {
        $reply = new Comment();
        $reply->body = $request->get('comment_body');
        $reply->user()->associate($request->user());
        $reply->parent_id = $request->get('comment_id');
        $post = Posting::find($request->get('post_id'));

        $post->comments()->save($reply);

        return back();

    }

    public function update(Request $request, $id)
    {
        $comment = Comment::find($id);
        $comment->body = $request->commentnew;
        
        $comment->update();
        return back()->with('status-alert', 'sukses-update-komen');
    }

    public function commentdestroy($id)
    {
        $comment = Comment::findOrFail($id);
        $comment->delete();

        return back()->with('status-alert', 'sukses-hapus-komen');
    }
}
