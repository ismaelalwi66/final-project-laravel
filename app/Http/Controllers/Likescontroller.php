<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Posting;
use App\Comment;

class Likescontroller extends Controller
{
    public function store(Request $request, Posting $post)
    {
        if (Auth::user()->hasLike($post)) {

            Auth::user()->unlike($post);

        } else {

            Auth::user()->like($post);

        }

        return back()->with("Success", "You Are Like Post");
    }

    public function storecomment(Request $request, Comment $comment)
    {
        if (Auth::user()->hasLikecomment($comment)) {

            Auth::user()->unlikecomment($comment);

        } else {

            Auth::user()->likecomment($comment);

        }

        return back()->with("Success", "You Are Like Comment");
    }

}
