<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Alert;

class UserController extends Controller
{
    public function index($username)
    {
        $user = User::where('username', $username)->first();
        return view('pages.profile.index', compact('user'));
    }

    public function update(Request $request, $id)
    {   
        
        $data = $request->all();

        $request->validate([
            'username' => 'required|max:255|regex:/^\S*$/u|unique:users,username,'.$request->id.'',
            'email' => 'email|max:255|unique:users,email,'.$request->id.''
        ]);

        $useraccount = User::findOrFail($id);

        if ($request->password) {
            $data['password'] = bcrypt($request->password);
        }
        else {
            unset($data['password']);
        }

        $useraccount->update($data);

        Alert::message('BookMyShow TamilRokers','Message');

        return back()->with('status-alert', 'sukses');
    }

}
