<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Posting;
use Auth;
use File;

class PostingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect('/');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        if($request->caption != "" or $request->image != ""){

            $id = Auth::user()->id;
            $image = $request -> image;

            
            if($image != ""){
                $file_name = time() . '-' . $image -> getClientOriginalName();            
                $image->move('postings/img',$file_name);
            }
            else{
                $file_name = '';
            }

            if($request -> caption == ""){
                $caption = '';
            }
            else $caption = $request -> caption;

            Posting::create([
                'image' => $file_name,
                'caption' => $caption,
                'user_id' => $id

            ]);
        }
        else{
            $this->validate($request,[
                'image' => 'required',
                'caption' => 'required'
            ]);

            return redirect('/')->with('status-alert', 'gagal-posting');
        }


        return redirect('/')->with('status-alert', 'sukses');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $postingan = Posting::find($id);

        if($postingan == ""){
            return redirect('/');
        }

        return view('posting.show',compact('postingan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $postingan = Posting::find($id);

        if($postingan == ""){
            return redirect('/');
        }

        return view('posting.edit',compact('postingan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        
        if($request->caption == "" and $request->image == ""){

            if(Posting::find($id)->image != ''){

                Posting::find($id)->update([
                    'caption' => ''
                ]);
            }
            else{
                return redirect('/');
            }       
         
            
        }
        else
        {
            
            $image = $request -> image;
            $caption = $request -> caption;

            $captiondb = Posting::find($id)->caption;
            

            if($caption === $captiondb and $image != ""){

                $path = "Postings/img/"; 
                File::delete($path.$image);

                $file_name = time() . '-' . $image -> getClientOriginalName();            
                $image->move('postings/img',$file_name);

                Posting::find($id)->update([
                    'image' => $file_name
                ]);

                redirect('/');
            }
            else if($caption != $captiondb and $image != ""){
                $path = "Postings/img/"; 
                File::delete($path.$image);

                $file_name = time() . '-' . $image -> getClientOriginalName();            
                $image->move('postings/img',$file_name);

                Posting::find($id)->update([
                    'caption' => $caption,
                    'image' => $file_name
                ]);

                redirect('/');
            }

            else if ($image != '' && $caption == '') {
                $path = "Postings/img/"; 
                File::delete($path.$image);

                $file_name = time() . '-' . $image -> getClientOriginalName();            
                $image->move('postings/img',$file_name);

                Posting::find($id)->update([
                    'image' => $file_name
                ]);

                redirect('/');
            }
            else{
                Posting::find($id)->update([
                    'caption' => $caption
                ]);
            }

            
            // if($image != "" and $caption !="" ){
            //     $file_name = time() . '-' . $image -> getClientOriginalName();            
            //     $image->move('postings/img',$file_name);
            // }
            // else{
            //     $file_name = '';
            // }

            // if($request -> caption == ""){
            //     $caption = '';
            // }
            // else $caption = $request -> caption;

            // Posting::create([
            //     'image' => $file_name,
            //     'caption' => $caption,
            //     'user_id' => $id

            // ]);
        }


        return back()->with('status-alert', 'sukses-diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {           
        $image = Posting::find($id)->image;
        $path = "Postings/img/"; 
        File::delete($path.$image);
        Posting::destroy($id);

        return redirect('/')->with('status-alert', 'sukses-dihapus');

    }
}
