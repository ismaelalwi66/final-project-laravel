<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profile;
use App\Following;
use App\User;
use App\Posting;
use App\Comment;
use Auth;



class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(User $follow)
    {
        // $posting = Posting::where('user_id', Auth::user()->id)
        // ->orderBy('created_at', 'DESC')->get();
        // $profname = Profile::all();

        $id = Auth::user()->id;

        $user = User::where('id',$id)->first();
        $following = Following::where('user_id',$id)->get();
        $follower = Following::where('following_user_id',$id)->get();
        $posting = Posting::orderBy('created_at', 'DESC')->get();
        $commentcount = Comment::where('commentable_id');
        $sugestuser = User::where('id', '!=', Auth::user()->id)
        ->orderBy('id', 'desc')
        ->take(5)
        ->get();
        
        return view('index',compact('user','following','follower','posting','id', 'commentcount', 'sugestuser'));
    }

    public function show()
    {
       
    }
}


