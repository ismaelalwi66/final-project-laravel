<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['middleware' => ['auth']], function () {


Route::get('/', 'HomeController@index')->name('beranda');


Route::resource('/posting','PostingController');
    

Route::get('/home', 'HomeController@index')->name('home');

Route::put('/profile/edit-account/{id}', 'UserController@update')->name('user.update');


Route::resource('profile', 'ProfileController')->only(['index', 'update']);
Route::get('profile/following', 'FollowingController@following')->name('profile.following');
Route::get('profile/follower', 'FollowingController@follower')->name('profile.follower');
Route::post('/profile/{user}', 'FollowingController@store')->name('following.store');


Route::get('/find-friend', 'ProfileController@findfriend')->name('find-friend');
Route::get('/find-friend/{id}', 'ProfileController@findfrienddetails')->name('friend-details');


Route::post('/comment/store', 'CommentController@store')->name('comment.add');
Route::post('/reply/store', 'CommentController@replyStore')->name('reply.add');
Route::put('/comment/{id}', 'CommentController@update')->name('comment.update');
Route::delete('/comment/{id}', 'CommentController@commentdestroy')->name('comment.delete');

Route::post('/like/{post}', 'LikesController@store')->name('like.store');
Route::post('/like/comment/{comment}', 'LikesController@storecomment')->name('likecomment.store');

Route::get('/followers/{id}', 'FollowingController@followers')->name('followers.followers');
Route::get('/following/{id}', 'FollowingController@following')->name('following.following');



});

Auth::routes();
