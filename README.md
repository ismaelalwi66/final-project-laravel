## Final Project Laravel

## Kelompok 20

## Anggota Kelompok


	Ismail Alwi (@ismaelalwi) ,Pengerjaan tugas :

	ERD
	Database Migration 
	Templating
	Setting model Home,Posting,Following 
	CRUD Posting,Follower,Following 
	Eloquent ORM Posting,Follower,following,user,profile



	Aldy Victor Rianes (@aldyvictor) ,Pengerjaan tugas :
	Database Migration 
	Templating
	Setting model Comment,Likecomment,Likepost
	CRUD Comment
	Fitur Follow user
	Fitur Like Post
	Fitur Like Comment 
	Eloquent ORM Comment,Likecomment,Likepost,user,profile,Posting


	Yazky Maulana Fajar Aji Saputra (@ayzky) ,Pengerjaan tugas :
	Deploy Heroku
	templating login, register
	modeling User, Profile
	Crud Profile
	Fitur multiple Login data, Username dan Email
	Package laravel datatables, sweet alert, tinymce




## Tema Project

Social Media

## ERD

<img src="public/erd/erd.png">

## Link Website
Website : <a href="http://stormy-beyond-21523.herokuapp.com/">JCC Media</a>

## Link Vidio
GDrive : <a href="https://drive.google.com/file/d/1GMU-n93WWwmajlLIOxTtbmpOtie1Hcsq/view?usp=sharing">JCC Media GDrive</a>


